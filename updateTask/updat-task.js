

async function updateTheTask(taskID, body) {
    await taskPut(taskID)(body);
}

function theNode(RED) {
     function UpdateTaskNode(config) {
        const ENTERPRISE = "aya";
        const API = require("../helpers/api");
        let api = new API(ENTERPRISE);
        const taskPut = api.update("task"); // taskPut(id)({...body})
        RED.nodes.createNode(this, config);
        var node = this;
         node.on('input', async function (msg) {
            msg.payload =  await taskPut(msg.payload)( { status: config.status }); // await updateTheTask(msg.payload, { status: "open" })
            // msg.payload = msg.payload.toLowerCase();
            node.send(msg);
        });
    }
    RED.nodes.registerType("updat-task", UpdateTaskNode);
}
module.exports = theNode