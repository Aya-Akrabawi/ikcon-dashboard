function theNode(RED) {
     function UpdateTaskNode(config) {
        const ENTERPRISE = "aya";
        const API = require("../helpers/api");
        let api = new API(ENTERPRISE);
        const taskPut = api.update("task"); // taskPut(id)({...body})
        RED.nodes.createNode(this, config);
        var node = this;
         node.on('input', async function (msg) {
                console.log("config", config);
             console.log("msg",msg);
            msg.payload =  await taskPut(msg.payload)( { stage: config.stage }); // await updateTheTask(msg.payload, { status: "open" })
            node.send(msg);
            
             
        });
    }
    RED.nodes.registerType("updat-stage", UpdateTaskNode);
}
module.exports = theNode