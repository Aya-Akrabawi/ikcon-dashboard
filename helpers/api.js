const moment = require("moment-timezone")
const requestretry = require('requestretry')
require('dotenv').config();
const dotenv = require('dotenv');

request = requestretry
request.defaults({
    maxAttempts: 1,
    fullResponse: true,
})

const isProd = process.env.NODE_ENV == "stg"
const HOOK = "https://api.dev.fieldradar.io/workforce/api/2.3.0"
// console.log('hook',HOOK);
/**
 * @param  {Null | Object} err
 * @param  {Object} response 
 * @param  {Object} body 
 * @param  {Object} options copy 
 * @return {Boolean} true if the request should be retried
 */
 function myRetryStrategy(err, response, body, options){
  // retry the request if we had an error or if the response was a 'Bad Gateway'
  return !!err || response.statusCode !== 200;
}

function getExponentialBackoff(attempts) {
    return (Math.pow(2, attempts) * 1000)
}

function constructExponentialBackoffStrategy() {
  let attempts = 0;
  return () => {
    attempts += 1;
    return getExponentialBackoff(attempts);
};
}

const retryOptions = {
    maxAttempts: 3,
    fullResponse: true,
    retryStrategy: myRetryStrategy
}


function todayQuery(opts) {
    if(opts.startDate && opts.endDate) {
        return opts;
    }
    // TODO: should i check the timezone for each enterprise?
    let startDate = moment.utc().startOf("day").valueOf();
    const endDate = moment.utc().valueOf();
    return { startDate, endDate };
}

class API {
    constructor(enterprise) {
        this.hookLink = process.env.API_HOOK
        this.enterprise = enterprise
        
        this.headers = {
            'Authorization': `Basic ${Buffer.from('System.nestrom:2DqpP2VGGQG').toString('base64')}`,
            "Content-Type": "application/json"            
        }
        this.nestromUser = `&user=ffffffffffffffffffffffff` ; 
    }

    async getEnterpriseInfo() {
        if(this.enterpriseInfo) return this.enterpriseInfo
            let endpoint = `enterprise/${this.enterprise}`;
        let url = `${HOOK}/${endpoint}`;

        var options = {
            url: url,
            method: "GET",
            headers: this.headers,
            json: true,
        };

        let response = await request(options);
        this.enterpriseInfo = response.body
        return response.body;
    }

    async _getTasks(opts, cb) {
        let endpoint = "task/getAllTasks";
        let p = opts.pagination || { start: 0, length: 6000 }
        let query = `start=${p.start}&length=${p.length}&user=601fc651136dac04905148a9&${opts.query || ""}`;
        let url = `${HOOK}/${endpoint}?${query}`;
        console.log('url',url);

        let data = {
            deleted: false,
            enterprise: this.enterprise,
            ...(opts.body || {})
        }
        console.log('data',data);

        let options = {
             headers : {
            Authorization: `Basic ${Buffer.from(`DCC.nestrom:nestrom@Nestrom`).toString("base64")}`,
            "Content-Type": "application/json"            
        }, 
        url, 
        json: data, 
        method: "POST" 
    };
        let response = await request(options);
        return response.body;
    }

    async getTasks(opts, cb) {
        return await this.requester("_getTasks", opts || {}, cb)
    }

    async _getTasksAnswers(opts, cb) {
        let endpoint = "task/tasksAnswers";
        let url = `${HOOK}/${endpoint}/${opts.params.templateId}`;

        var options = {
            url: url,
            method: "POST",
            headers: this.headers,
            json: true,
            body: opts.body || {}
        };

        let response = await request(options);
        return response.body;
    }

    async getTasksAnswers(opts, cb) {
        return await this.requester("_getTasksAnswers", opts || {}, cb)
    }

    async _getOneAnswer(answerId, cb) {
        let endpoint = "task/answer";
        let url = `${HOOK}/${endpoint}/${answerId}`;

        var options = {
            url: url,
            method: "GET",
            headers: this.headers,
            json: true
        };

        let response = await request(options);
        return response.body;
    }

    async getOneAnswer(answerId, cb) {
        return await this.requester("_getOneAnswer", answerId, cb)
    }

    async _getTemplate(opts, cb) {
        let endpoint = "form";
        let { templateId } = opts.params;
        let url = `${HOOK}/${endpoint}/${templateId}`;

        let options = {
            url,
            headers: this.headers,
            json: true
        };

        let response = await request(options);
        return response.body;
    }

    async getTask(taskId) {
        let url = `${HOOK}/task/${taskId}`;
        let options = { url, headers: this.headers, json: true };
        let response = await request(options);
        return response.body;
    }

    async getTemplate(opts, cb) {
        return await this.requester("_getTemplate", opts || {}, cb)
    }

    async getObservations(opts, cb) {
        let response = await this._getObservations(opts, cb);
        if (cb && typeof cb === 'function') {
            return cb(response);
        }
        return response;
    }
        // generic insert
        insert (entity) {
            let headers = this.headers
            return async function (body) {
                const response = await request({headers ,json: body, method: "POST", url: `${HOOK}/${entity}`})
                // console.log('response.statusCode', response.statusCode)
                if (response.statusCode != 200 && response.statusCode != 201) {
                    throw new Error(`${JSON.stringify(response.body)} Something went wrong while trying to insert a new doc in entity ${entity} - ${JSON.stringify(body)}`);
                }
                return response.body;
            }
        }
 update (entity) {
        let headers = this.headers
        return function (id) {
            return async function (body) {
                const response = await request({headers ,json: body, method: "PUT", url: `${HOOK}/${entity}/${id}`})
                if (response.statusCode != 200) {
                    throw new Error(`${JSON.stringify(response.body)} Cannot update ${entity} of ${id}`);
                }
                return response.body;
            }
        }
    }
    async getOneObservation(id) {
        let endpoint = "observation/rendered";
        let url = `${HOOK}/${endpoint}/${id}`;

        let options = {
            url,
            headers: this.headers,
            json: true
        };

        let response = await request(options);
        return response.body;
    }
    async getOneDox(id) {
        let endpoint = "dox/rendered";
        let url = `${HOOK}/${endpoint}/${id}`;

        let options = {
            url,
            headers: this.headers,
            json: true
        };

        let response = await request(options);
        return response.body;
    }

    async _getObservations(opts, cb) {
        let endpoint = "observation/search";
        let query = `start=0&length=500${this.nestromUser}&${opts.query}`;
        if(opts.pageLength) {
            let len = opts.pageLength || 50;
            query = `start=${opts.page * len}&length=${len}${this.nestromUser}&${opts.query}`;
        }
        let url = `${HOOK}/${endpoint}/${this.enterprise}?${query}`;
        console.log("url", url)

        let options = { headers: this.headers, url, json: true };
        let response = await request(options);
        if (!response.body.data) {
          return response.body;
      }

      if(opts.getFullObject){
        return response.body 
    }
    return response.body.data;
}

async findObservations(opts) {
    let endpoint = "observation/find";
    let query = `${this.nestromUser}&${opts.query}`;
    let url = `${HOOK}/${endpoint}/${this.enterprise}?${query}`;

    let options = { headers: this.headers, url, json: true };
    let response = await request(options);
    return response.body;
}

async findDox(opts) {
    let endpoint = "dox/find";
    let query = `${this.nestromUser}&${opts.query}`;
    let url = `${HOOK}/${endpoint}/${this.enterprise}?${query}`;

    let options = { headers: this.headers, url, json: true };
    let response = await request(options);
    return response.body;
}

async getDox(opts, cb) {
    let response = await this._getDox(opts);
    if (cb && typeof cb === 'function') {
        return cb(response);
    }
    return response;
}

async _getDox(opts) {
    let endpoint = "dox/search"
    let query = `start=0&length=500${this.nestromUser}&${opts.query}`;
    if(opts.pageLength) {
        let len = opts.pageLength || 50;
        query = `start=${opts.page * len}&length=${len}${this.nestromUser}&${opts.query}`;
    }

    let url = `${HOOK}/${endpoint}/${this.enterprise}?${query}`;
    console.log("url", url)
    let options = { headers: this.headers, url, json: true };
    let response = await request(options);
    if (!response.body.data) {
        return response.body;
    }
    if(opts.getFullObject){
        return response.body 
    }
    return response.body.data;
}


async requester(func, opts, cb) {
        // TODO: check status and change the cb to (error, data)
        let response = await this[func](opts, cb);
        if (cb && typeof cb === 'function') {
            return cb(response);
        }
        return response;
    }

    getPDF(url){
        console.log("url", url)
        return request({
            method: "GET",
            json: true,
            url,
            encoding: null,
            baseUrl: process.env.REPORTS_BASE_URL,
            ...retryOptions,
            delayStrategy: constructExponentialBackoffStrategy()
        }); 
    }
    getTaskPDF(taskId, lang){
        let url = `custom/allTask/${this.enterprise}/${taskId}/${lang}`
        return this.getPDF(url);
    }
    getObservationPDF(observationId, lang){
        let url = `custom/observation/${this.enterprise}/${observationId}/${lang}`
        return this.getPDF(url);
    }
    getDoxPDF(doxId, lang){
        let url = `custom/dox/${this.enterprise}/${doxId}/${lang}`
        return this.getPDF(url);
    }

    async getTodayTasks(opts) {
        let { startDate, endDate } = todayQuery(opts)
        let json = { creationDate: { startDate, endDate }, ...(opts.body || {}) };
        const body = await this._getTasks({ ...opts, body: json })
        return body;
    }

    async getTodayObservations(opts) {
        let { startDate, endDate } = todayQuery(opts)
        let query = `startDate=${startDate}&endDate=${endDate}&${(opts.query || "")}`;
        let body = await this._getObservations({ ...opts, query })
        return body;
    }

    async getTodayDox(opts) {
        let { startDate, endDate } = todayQuery(opts)
        let query = `startDate=${startDate}&endDate=${endDate}&${(opts.query || "")}`;
        let body = await this._getDox({ ...opts, query })
        return body;
    }

    async getForms(type) {
        let endpoint = "/form/search";
        let query = `${this.nestromUser}&scroll=true`;
        let url = `${HOOK}/${endpoint}/${this.enterprise}`;
        if(type)
            url = `${url}/${type}`
        let options = { headers: this.headers, url : `${url}?${query}`, json: true };
        let response = await request(options);
        return response.body.data;
    }
    // generic getter
    get(entity) {
        let endpoint = `/${entity}`;
        return async (id) => {
            let url = `${HOOK}/${endpoint}/${id}`;
            const response = await request({headers: this.headers, url})
            if (response.statusCode != 200) {
                throw new Error(`Cannot get ${entity} of ${id}`);
            }
            return typeof response.body == 'object' ? response.body : JSON.parse(response.body);
        }
    }

    // type dox or observation
    async tableSearch(type, id, version, query = "") {
        let endpoint = `tableSearch/${id}/${version}`
        let url = `${HOOK}/${type}/${endpoint}?start=0&length=1000&${query}${this.nestromUser}`
        let opts = {headers: this.headers, url}
        const response = await request(opts)
        if (response.statusCode != 200) {
            throw new Error(response.body);
        }
        return typeof response.body == 'object' ? response.body : JSON.parse(response.body);
    }
}

module.exports = API
