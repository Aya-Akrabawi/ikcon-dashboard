function theNode(RED) {
     function UpdateTaskNode(config) {
        const ENTERPRISE = "aya";
        const API = require("../helpers/api");
        let api = new API(ENTERPRISE);
        const taskPut = api.update("task"); // taskPut(id)({...body})
        RED.nodes.createNode(this, config);
        var node = this;
         node.on('input', async function (msg) {
                console.log("config", config);
             console.log("msg",msg);
             let body = {
                addingTemplates: msg.payload.addingTemplates ? msg.payload.addingTemplates : config.taskAddingTemplates,
                allowedTemplates: msg.payload.allowedTemplates ? msg.payload.allowedTemplates : [config.taskAllowedTemplates].flat(),
                assignedUser: msg.payload.assignedUser ? msg.payload.assignedUser : config.taskAssignedUser,
                associatedUsers: msg.payload.associatedUsers ? msg.payload.associatedUsers : [config.taskAssociatedUsers].flat(),
                criticalLevel: msg.payload.criticalLevel ? msg.payload.criticalLevel : config.taskCriticalLevel,
                endDate: msg.payload.endDate ? Date.parse(msg.payload.endDate) : Date.parse(config.taskEndDate),
                enterprise: "aya", // msg.payload.enterprise ? msg.payload.enterprise : config.taskEnterprise,
                files: msg.payload.files ? msg.payload.files : [config.taskFiles].flat(),
                folders: msg.payload.folders ? msg.payload.folders : [config.taskFolders].flat(),
                name: msg.payload.taskName ? msg.payload.taskName : config.taskName,
                repeatType: msg.payload.repeatType ? msg.payload.repeatType : config.taskRepeatType,
                reportedBy: msg.payload.reportedBy ? msg.payload.reportedBy : config.taskAssignedUser,
                requiredAtLocation: msg.payload.requiredAtLocation ? msg.payload.requiredAtLocation : config.taskRequiredAtLocation,
                startDate: msg.payload.startDate ? Date.parse(msg.payload.startDate) : Date.parse(config.taskStartDate),
                status: msg.payload.status ? msg.payload.status : config.taskStatus,
                tags: msg.payload.tags ? msg.payload.tags : [config.taskTags].flat(),
                templates: msg.payload.templates ? msg.payload.templates : [
                        {
                            isRequired: config.taskRequiredTemplate,
                            templateId: config.taskTemplates,
                        },
                    ],
                type: msg.payload.taskType ? msg.payload.taskType : config.taskType,
                description: msg.payload.description ? msg.payload.description : config.taskDescription,
                // validation: false
                // taskId: taskDetails._id,
                // requiredAtLocation: false,
                // assignedUser: values.ASSIGN_CAPA_TO ? values.ASSIGN_CAPA_TO : taskDetails.assignedUser._id,
                // associatedUsers: [`${taskDetails.assignedUser._id}`],
                // startDate: moment().utc().valueOf(),
                // endDate: moment().utc().add(24, "hours").valueOf(),
                // criticalLevel: "low",
                // enterprise: ENTERPRISE,
                // name: "CAPA Task",
                // refId: `${taskDetails.refId}`,
                // repeatType: "assigned",
                // reportedBy: taskDetails.assignedUser._id,
                // status: "open",
                // addingTemplates: false,
                // description: `<p><div><b>Link of the task: </b> <pre>${process.env.MAIN_PDF}${ENTERPRISE}/${taskDetails._id}/en</pre></div></p>`,
                // templates: [
                //     {
                //         isRequired: true,
                //         templateId: CONFIGS.capa.TEMPLATES.CAPA_FORM.TEMPLATEID,
                //     },
                // ],
                // type: CONFIGS.capa.TASK_TYPES.CAPA,
            };
            if (msg.organization) body.organization = msg.taskOrganization;
            if (msg.location) body.location = msg.taskLocation;
            if (msg.zone) body.zones = [msg.taskZone];
            if (msg.assetId) body.assetId = msg.taskAssetId;
            if (body.templates && body.templates.length !== 0 && body.templates[0].templateId === '') {
                body.templates = []
            }
            // if (msg.refId) body.refId = ms.refId;
            console.log("body", body);
            // try {
            //  msg.payload = await api.insert("task")(body)
            // } catch (error) {
            // console.error(error);
            // msg.payload = error
            // }
            node.send(msg);
            
             
        });
    }
    RED.nodes.registerType("create-task", UpdateTaskNode);
}
module.exports = theNode