function theNode(RED) {
    function UpdateTaskNode(config) {
        const ENTERPRISE = "aya";
        const API = require("../helpers/api");
        let api = new API(ENTERPRISE);
        RED.nodes.createNode(this, config);
        var node = this;
        node.on('input', async function (msg) {
            console.log("config", config);
            console.log("msg", msg);
            try {
             msg.payload = await api.get(config.getType)(msg.id)
            } catch (error) {
            console.error(error);
            msg.payload = error
            }
            node.send(msg);


        });
    }
    RED.nodes.registerType("get-component", UpdateTaskNode);
}
module.exports = theNode